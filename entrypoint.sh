#!/bin/bash
echo "Running entrypoint..."
exec node main.js > >(rotatelogs -e -t /logs/stdout.log 5M) 2> >(rotatelogs -e -t /logs/errors.log 5M >&2 )