FROM node:6

RUN mkdir /logs && \
    apt-get update && \
    apt-get install -y apache2-utils && \
    rm -rf /var/lib/apt/lists/*
VOLUME "/logs"

COPY main.js .
COPY entrypoint.sh /bin/entrypoint.sh

CMD ["/bin/entrypoint.sh"]